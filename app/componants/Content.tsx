"use client";
import axios from "@/node_modules/axios/index";
import { useEffect, useMemo, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Col from "@/node_modules/react-bootstrap/esm/Col";
import Container from "@/node_modules/react-bootstrap/esm/Container";
import Row from "@/node_modules/react-bootstrap/esm/Row";
import Card from "@/node_modules/react-bootstrap/esm/Card";
import Button from "@/node_modules/react-bootstrap/esm/Button";
import Image from "@/node_modules/react-bootstrap/esm/Image";
import DropdownButton from "@/node_modules/react-bootstrap/esm/DropdownButton";
import Dropdown from "@/node_modules/react-bootstrap/esm/Dropdown";
import { Pagination } from "@/node_modules/react-bootstrap/esm/index";

type props = {
  setSearching: any;
  searching: any;
  itemInCart: any;
  setItemInCart: any;
};

export default function Content({
  setSearching,
  searching,
  itemInCart,
  setItemInCart,
}: props) {
  const [infos, setInfos] = useState([]);
  const [page, setPage] = useState(0);
  const [rarityInfo, setRarityInfo] = useState([]);
  const [typeInfo, setTypeInfo] = useState([]);
  const [setsInfo, setSetInfo] = useState([]);
  const [onFilterBySet, setOnFilterBySet] = useState('');
  const [onFilterByRarity, setOnFilterByRarity] = useState("");
  const [onFilterByType, setOnFilterByType] = useState<any>("");
  // const [dataOnSet, setDataOnSet] = useState([]);

  useMemo(() => {
    // console.log("setNewUrl",setNewUrl);
    axios({
      method: "get",
      url: `https://api.pokemontcg.io/v2/cards?page=1&pageSize=20${
        searching && `&q=name:${searching}*`
      }`,
      responseType: "json",
    }).then(function (response) {
      // console.log("response",response?.data.totalCount/20);
      // const data = [] as any;
      // for (let i = 0; i < 20; i++) {
      //   data.push(response?.data?.data[i]);
      // }
      setPage((response?.data.totalCount)/20)
      setInfos(response?.data?.data);
    });

    axios({
      method: "get",
      url: "https://api.pokemontcg.io/v2/rarities",
      responseType: "json",
    }).then(function (response) {
      setRarityInfo(response?.data?.data);
    });

    axios({
      method: "get",
      url: "https://api.pokemontcg.io/v2/sets",
      responseType: "json",
    }).then(function (response) {
      setSetInfo(response?.data?.data);
    });

    axios({
      method: "get",
      url: "https://api.pokemontcg.io/v2/types",
      responseType: "json",
    }).then(function (response) {
      setTypeInfo(response?.data?.data);
    });
  }, [searching]);

  useEffect(() => {
    axios({
      method: "get",
      url: `https://api.pokemontcg.io/v2/cards?page=1&pageSize=20&q=${
        onFilterBySet && `set.id:${onFilterBySet}`
      }`,
      responseType: "json",
    }).then(function (response) {
      // const data = [] as any;
      // for (let i = 0; i < 20; i++) {
      //   data.push(response?.data?.data[i]);
      // }
      setInfos(response?.data?.data);
    });
  }, [onFilterBySet]);
  useEffect(() => {
    axios({
      method: "get",
      url: `https://api.pokemontcg.io/v2/cards?page=1&pageSize=20&q=${
        onFilterByType && `types:${onFilterByType}`
      }`,
      responseType: "json",
    }).then(function (response) {
      // const data = [] as any;
      // for (let i = 0; i < 20; i++) {
      //   data.push(response?.data?.data[i]);
      // }
      setInfos(response?.data?.data);
    });
  }, [onFilterByType]);
  useEffect(() => {
    axios({
      method: "get",
      url: `https://api.pokemontcg.io/v2/cards?page=1&pageSize=20&q=${
        onFilterByRarity && `&rarity:${onFilterByRarity}`
      }`,
      responseType: "json",
    }).then(function (response) {
      // const data = [] as any;
      // for (let i = 0; i < 20; i++) {
      //   data.push(response?.data?.data[i]);
      // }
      setInfos(response?.data?.data);
    });
  }, [onFilterByRarity]);

  function onAddCart(item: any) {
    let infos = {
      id: item.id,
      name: item.name,
      cardmarket: item.cardmarket,
      images: item.images,
      amount: 1,
    };
    setItemInCart((itemInCart: any) => [infos, ...itemInCart]);
    localStorage.setItem("cart", JSON.stringify(itemInCart));
  }

  return (
    <>
      <Container>
        <Row>
          <Col md={6} sm={12}>
            <h3>Choose Card</h3>
          </Col>
          <Col md={6} sm={12} className="ms-auto">
            <Row className="text-end">
              <Col col={6}></Col>
              <Col col={2}>
                <DropdownButton id="dropdown-basic-button" title="Set">
                  {setsInfo?.map((item : any) => (
                    <Dropdown.Item onClick={() => setOnFilterBySet(item?.id)}>
                      {item?.name}
                    </Dropdown.Item>
                  ))}
                </DropdownButton>
              </Col>
              <Col col={2}>
                <DropdownButton id="dropdown-basic-button" title="Rarity">
                  {rarityInfo?.map((item) => (
                    <Dropdown.Item
                      key={item}
                      onClick={() => setOnFilterByRarity(item)}
                    >
                      {item}
                    </Dropdown.Item>
                  ))}
                </DropdownButton>
              </Col>
              <Col col={2}>
                <DropdownButton id="dropdown-basic-button" title="Type">
                  {typeInfo?.map((item) => (
                    <Dropdown.Item onClick={() => setOnFilterByType(item)}>
                      {item}
                    </Dropdown.Item>
                  ))}
                </DropdownButton>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row>
          {infos?.map((info: any) => (
            <Col md={2} sm={12} className="my-1 mt-5">
              <div className="pt-5">
                <Card
                  style={{
                    width: "176px",
                    backgroundColor: "#1F1D2B",
                    color: "#ffffff",
                    fontSize: "12px",
                  }}
                  className="pt-3 text-center text-white"
                >
                  <Card.Img className="card-img" src={info?.images?.small} />
                  <Card.Body>
                    <p>{info?.name}</p>
                    <Card.Text>
                      ${info?.cardmarket?.prices?.averageSellPrice} .{" "}
                      {info?.set?.total} Card
                    </Card.Text>
                    <button
                      className="cart-btn"
                      onClick={(e) => onAddCart(info)}
                    >
                      <Image
                        src="/shopping-bag.png"
                        height={22}
                        className="mx-2 p-1"
                      />
                      Add to cart
                    </button>
                  </Card.Body>
                </Card>
              </div>
            </Col>
          ))}
        </Row>
      </Container>
    </>
  );
}
