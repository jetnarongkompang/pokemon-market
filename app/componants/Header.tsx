"use client";
import {
  FormControl,
  Input,
  InputAdornment,
  InputLabel,
  TextField,
} from "@/node_modules/@mui/material/index";
import Container from "@/node_modules/react-bootstrap/esm/Container";
import { Button, Modal } from "@/node_modules/react-bootstrap/esm/index";
import { Col, Row } from "@/node_modules/react-bootstrap/esm/index";
import SearchIcon from "@/node_modules/@mui/icons-material/Search";
import Image from "@/node_modules/react-bootstrap/esm/Image";
import { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { faXing } from "@/node_modules/@fortawesome/free-brands-svg-icons/index";

type props = {
  setSearching: any;
  searching: any;
  itemInCart: any;
  setItemInCart: any;
};

export default function Header({
  setSearching,
  searching,
  itemInCart,
  setItemInCart,
}: props) {
  const [isShow, setIsShow] = useState(false);
  const handleClose = () => setIsShow(false);
  const handleShow = () => setIsShow(true);
  const [allCard, setAllCard] = useState(0);
  const [allPrice, setAllPrice] = useState("0.00");
  const [cartInfos, setCartInfos] = useState<any>([]);

  useEffect(() => {
    const retString = localStorage.getItem("cart");

    if (retString) {
      let cartInfo = JSON.parse(retString || "");
      setCartInfos(cartInfo);
    }
  }, [itemInCart]);

  useEffect(() => {
    let price = 0;
    let amount = 0;
    cartInfos?.map(
      (item: any) =>
        (price = price + item?.cardmarket?.prices?.averageSellPrice)
    );

    cartInfos?.map((item: any) => (amount = amount + item.amount));
    setAllPrice(String(price.toFixed(2)));
    setAllCard(amount);
    setCartInfos(cartInfos);
  }, []);

  function handleChange(e: any) {
    setSearching(e);
  }

  function onClearCart() {
    localStorage.clear();
    setCartInfos([]);
    setItemInCart([]);
    setAllPrice("0.00");
    setAllCard(0);
    setIsShow(false);
  }

  function onClickPlus(item: any) {
    let total = 0;
    let totalPrice = 0;
    cartInfos.forEach((element: any) => {
      if (element.id == item.id) {
        element.amount = element.amount + 1;
      }
    });

    cartInfos.forEach((element: any) => {
      total = total + element.amount;
      totalPrice =
        totalPrice +
        element?.cardmarket?.prices?.averageSellPrice * element.amount;
    });

    setAllPrice(totalPrice.toFixed(2));
    setAllCard(total);

    localStorage.clear();
    localStorage.setItem("cart", JSON.stringify(cartInfos));
    setCartInfos(cartInfos);
  }

  function onClickDelete(item: any) {
    let total = 0;
    let totalPrice = 0;
    cartInfos.forEach((element: any) => {
      if (element.id == item.id) {
        if (element.amount >= 0) {
          element.amount = element.amount - 1;
        }
      }
    });

    cartInfos.forEach((element: any) => {
      total = total + element.amount;
      totalPrice =
        totalPrice +
        element?.cardmarket?.prices?.averageSellPrice * element.amount;
    });

    setAllPrice(totalPrice.toFixed(2));
    setAllCard(total);

    localStorage.clear();
    localStorage.setItem("cart", JSON.stringify(cartInfos));
    setCartInfos(cartInfos);
  }

  return (
    <>
      <div className="modal-position-custom">
        <Modal
          show={isShow}
          onHide={handleClose}
          dialogClassName={"modal-custom"}
          contentClassName={"modal-content-custom"}
        >
          <Modal.Body>
            <Row>
              <Col sm={6}>
                <h3>Cart</h3>
                <div>
                  <Button onClick={onClearCart} variant="link">
                    Clear all
                  </Button>
                </div>
              </Col>
              <Col sm={6} className="text-end p-2">
                <button className="cart-btn-header" onClick={handleClose}>
                  X
                </button>
              </Col>
              <Col sm={2}>Item</Col>
              <Col sm={6}>Qty</Col>
              <Col sm={4}>Price</Col>
            </Row>
            {cartInfos?.map((item: any) => (
              <Row>
                <Col sm={2}>
                  <Image
                    src={item?.images?.small}
                    height={60}
                    // className="mx-2 p-1"
                  />
                </Col>
                <Col sm={6}>
                  {item?.name}
                  <div>$ {item?.cardmarket?.prices?.averageSellPrice}</div>
                </Col>
                <Col sm={4}>
                  ${" "}
                  {(
                    parseFloat(item?.cardmarket?.prices?.averageSellPrice) *
                    item.amount
                  ).toFixed(2)}
                </Col>
                <Col sm={2} className="my-2">
                  <button
                    className="cart-btn-cart"
                    onClick={(e) => onClickDelete(item)}
                  >
                    -
                  </button>
                </Col>
                <Col sm={6} className="my-2">
                  <button className="cart-btn-cart w-100">{item.amount}</button>
                </Col>
                <Col sm={4} className="p-0 my-2">
                  <button
                    className="cart-btn-cart"
                    onClick={(e) => onClickPlus(item)}
                  >
                    +
                  </button>
                </Col>
              </Row>
            ))}
            {!cartInfos ? (
              <Row>
                <Col sm={12} className="h-20"></Col>
              </Row>
            ) : (
              <></>
            )}
            <Row>
              <Col sm={12}></Col>
              <Col sm={6} className="text-start">
                Total card amount
              </Col>
              <Col sm={6} className="text-end">
                {allCard}
              </Col>
              <Col sm={6} className="text-start">
                Total price
              </Col>
              <Col sm={6} className="text-end">
                $ {allPrice}
              </Col>
              <Col sm={12} className="text-center">
                <button className="cart-btn-header w-100" onClick={handleClose}>
                  Continue to Payment
                </button>
              </Col>
            </Row>
          </Modal.Body>
        </Modal>
      </div>

      <Container>
        <Row className="pt-5">
          <Col md={6} sm={12}>
            <h1>Pokemon market</h1>
          </Col>
          <Col md={6} sm={12} className="text-end">
            <TextField
              id="input-with-icon-textfield"
              placeholder="Search"
              color="primary"
              className="search-box-customize pr-2"
              onChange={(e: any) => {
                handleChange(e.target.value);
              }}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <SearchIcon style={{ color: "white" }} />
                  </InputAdornment>
                ),
              }}
              sx={{
                // Root class for the input field
                "& .MuiOutlinedInput-root": {
                  color: "#ffffff",
                  fontFamily: "Arial",
                  // Class for the border around the input field
                  "& .MuiOutlinedInput-notchedOutline": {
                    borderColor: "#ffffff",
                    borderWidth: "1px",
                  },
                },
                // Class for the label of the input field
                "& .MuiInputLabel-outlined": {
                  color: "#2e2e2e",
                  fontWeight: "bold",
                },
              }}
              variant="outlined"
            />

            <button className="cart-btn-header" onClick={handleShow}>
              <Image src="/shopping-bag.png" height={24} className="mx-2 p-1" />
            </button>
          </Col>
        </Row>
        <hr />
      </Container>
    </>
  );
}
