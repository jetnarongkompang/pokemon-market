"use client";

import Header from "./componants/Header";
import Content from "./componants/Content";
import { useEffect, useState } from "react";

export default function Home() {
  const [searching, setSearching] = useState("");
  const [itemInCart, setItemInCart] = useState<any>([]);

  return (
    <div style={{ backgroundColor: "#252836" }} className="display-style">
      <Header
        searching={searching}
        setSearching={setSearching}
        itemInCart={itemInCart}
        setItemInCart={setItemInCart}
      ></Header>
      <Content
        searching={searching}
        setSearching={setSearching}
        itemInCart={itemInCart}
        setItemInCart={setItemInCart}
      ></Content>
    </div>
  );
}
